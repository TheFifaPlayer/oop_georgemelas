﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class DoorScript : MonoBehaviour 
{
	public Text doorOpenMessage;
	public bool door;
	public GameObject Door;
	public string DoorTag;
	public Text KeyNotMessage;




	// Use this for initialization
	void Start () 
	{
		door = false;
		doorOpenMessage.gameObject.SetActive(false);
		KeyNotMessage.gameObject.SetActive (false);


	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetMouseButton(1)) 
		{
			GetComponent<Animation> ().Play ("DoorClip");
			doorOpenMessage.gameObject.SetActive (true);
		}
	}


	public void OnTriggerEnter(Collider other)
	{
		{
			door = true;
			Door.gameObject.SetActive(true);

		}

}

	public void OnTriggerStay(Collider other)
	{
		{
			KeyNotMessage.gameObject.SetActive (true);
		}
	}




}