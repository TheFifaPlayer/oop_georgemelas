﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour 
{
	public float MovementSpeed;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		MovementButtons ();
	}

	public void MovementButtons()
	{
		if (Input.GetKey (KeyCode.W)) 
		{
			MoveForward();
		}
	

		if (Input.GetKey (KeyCode.A)) 
		{
			MoveLeft();
		}
	

		if (Input.GetKey (KeyCode.S)) 
		{
			MoveBack();
		}

		if (Input.GetKey (KeyCode.D)) 
		{
			MoveRight();
		}
	}


	public void MoveForward()
	{
		GetComponent<Rigidbody> ().AddForce (Vector3.forward * MovementSpeed);
	}

	public void MoveLeft()
	{
		GetComponent<Rigidbody> ().AddForce (Vector3.left* MovementSpeed);
	}

	public void MoveBack()
	{
		GetComponent<Rigidbody> ().AddForce (Vector3.back * MovementSpeed);
	}

	public void MoveRight()
	{
		GetComponent<Rigidbody> ().AddForce (Vector3.right * MovementSpeed);
	}
}

