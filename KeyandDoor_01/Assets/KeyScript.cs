﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KeyScript : MonoBehaviour 
{
	public bool isKeyCollected;
	public Image keyImage;
	public string KeyCollectedString = "Key Collected";
	public string playertag;
	public Text KeyCollectedtext;
	// Use this for initialization
	void Start () 
	{
		keyImage.gameObject.SetActive(false);
		KeyCollectedtext.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Key") 
		{
			GameObject.Destroy(other.gameObject);
			isKeyCollected = true;
			KeyCollectedtext.gameObject.SetActive(true);
			keyImage.gameObject.SetActive(true);
			print("KeyColl");


		}
	}




}
