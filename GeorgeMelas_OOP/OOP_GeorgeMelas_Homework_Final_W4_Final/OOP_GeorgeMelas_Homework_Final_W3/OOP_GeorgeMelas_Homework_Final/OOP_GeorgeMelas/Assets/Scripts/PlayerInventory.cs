﻿using UnityEngine;
using System.Collections;
[System.Serializable]


public struct Item
{
public string Name;
public string Description;
public string UseMessage;

}
public class PlayerInventory : MonoBehaviour 
{		
	public int StoringIndex;
	public Item[] Items;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		CheckKeys();
	}

	public void IncreaseIndex()
	{
		{
			StoringIndex ++;
			
			
			if(StoringIndex  >6)	
				
			{
				StoringIndex =0;
				Debug.LogWarning ("Positive numbers are out of range! Currentindex set to 10");
			}
			
		}	
	}



	public void DecreaseIndex()
	
	{
		{
			StoringIndex --;
			
			
			if(StoringIndex <0)	
				
			{
				StoringIndex =0;
				Debug.LogWarning ("Negative numbers are out of range! Currentindex set to 10");
			}
			
		}	
	
	}

	public void useItem()
	
	{
		print ("Items[StoringIndex].UseMessage");
	
	}

	public void CheckKeys() 
	{
		if(Input.GetKeyDown(KeyCode.LeftBracket))
		{
			IncreaseIndex();
		}
		
		if(Input.GetKeyDown(KeyCode.RightBracket))
		{
			DecreaseIndex(); 
		}
	
		if(Input.GetKeyDown(KeyCode.Space))
		{
			useItem();
		}
	}
}


