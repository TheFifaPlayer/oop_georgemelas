﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour {
	public int recoverAmount;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}


	private void destroyPickup()
	{
		Destroy (gameObject);
	}

	private void addHealth (Player targetPlayer)
	{
		targetPlayer.playerstats.Health += recoverAmount;
	}

	private void OnTriggerEnter(Collider col)
	{
		Player triggeredPlayer = col.gameObject.GetComponent<Player> ();
		if(triggeredPlayer != null)
		{
			addHealth(triggeredPlayer);
			destroyPickup();
		}
}

}