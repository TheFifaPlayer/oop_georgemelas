﻿using UnityEngine;
using System.Collections;
[System.Serializable]

public struct Stats
{
	public float Health;
	public string Name;
	public float MovementSpeed;
	public float jumpheight;
	public KeyBinds Control;

}
public class Player : MonoBehaviour 
{

	public Stats playerstats;



	void Start()
{
	
}

// Update is called once per frame
void Update () 
{
	CheckKeyPresses();
	
}

	public void CheckKeyPresses()
	{
	
		if(Input.GetKey(playerstats.Control.fowardkey))
	{
		print("W key was pressed");
		MovePlayerFoward();
	}
	
		if(Input.GetKey(playerstats.Control.backkey))
	{
		print("A Key Was pressed");
		MovePlayerBack();
	}
	
	
		if(Input.GetKey(playerstats.Control.leftkey))
	{
		print("S Key Was Pressed");
		MovePlayerLeft();
	}
	
		if(Input.GetKey(playerstats.Control.rightkey))
	{
		print("D Key Was Pressed");
		MovePlayerRight();
	}
	
	
	if(Input.GetKeyDown(playerstats.Control.upKey))
	{
		print("Space Key Was Pressed");
		Jump();
		
	}
	
}


	public void MovePlayerFoward()
	{	print ("Player is Moving Foward");
		GetComponent<Rigidbody>().AddForce(Vector3.forward * playerstats.MovementSpeed);
	
	}

	public void MovePlayerBack()
	{	print ("Player is Moving Back");
		GetComponent<Rigidbody>().AddForce(Vector3.back *playerstats.MovementSpeed);
	
	}

	public void MovePlayerLeft()
	{	print ("Player is Moving Left");
		GetComponent<Rigidbody>().AddForce(Vector3.left * playerstats.MovementSpeed);
	
	}


	public void MovePlayerRight()
	{	print ("Player is Moving Right");
		GetComponent<Rigidbody>().AddForce(Vector3.right * playerstats.MovementSpeed);
	
	}


	public void Jump()
	{	print ("Player is Jumping");
		GetComponent<Rigidbody>().AddForce(Vector3.up * playerstats.jumpheight);
	}

	
	}








	



