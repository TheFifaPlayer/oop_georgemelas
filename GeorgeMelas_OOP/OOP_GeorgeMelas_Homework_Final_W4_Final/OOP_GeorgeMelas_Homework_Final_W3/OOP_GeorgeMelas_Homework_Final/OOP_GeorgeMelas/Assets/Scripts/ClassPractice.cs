﻿using UnityEngine;
using System.Collections;

public class ClassPractice : MonoBehaviour 
{
	public int number;
	public float decimalNumber;
	public bool aBoolean;
	public string aWord;
	public string anotherWord;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	public void PrintNumber()
	{

		print (number);
	}

	public void PrintBool()
	{
		
		print (aBoolean);
	}

	public void PrintFloat()
	{

		print (decimalNumber);
	
	}

	public void PrintWord()
	{
		print (aWord);
	}


	public void PrintAnotherWord()
	{
		print (anotherWord);
	}
}


