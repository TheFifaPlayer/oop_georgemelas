﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public struct StructScope
{
	public int Num;
	public string Name;
	public int age;
}

public class ScopePractice : MonoBehaviour 
{
	public int ClassNumber;// Is visible due to being inside the class scope
	public bool exists;
	public string messageInABottle;
	public bool checkIf;
	public string notCheck;
	public StructScope StructVarible;
	private string SecretMessage = "Boo";
	
	// Use this for initialization
	void Start () 
	{
		bool exists;
		exists = true;

		this.exists = exists;
		scopeFunction ();

		StructVarible.Num = 2;
		StructVarible.Name = "George";
		StructVarible.age = 17;

	}
	
	// Update is called once per frame
	void Update () 
	{
		string messageInABottle;
		messageInABottle = "Message";
		this.messageInABottle = messageInABottle;



	}

	public void scopeFunction()
	{
		bool checkIf;
		checkIf = false;
		this.checkIf = checkIf;

		if(checkIf == false)
		{
			string notCheck;
			notCheck = "Not";
			this.notCheck = notCheck;
		}
	}


	public void printSecretMessage()
	{
		print (SecretMessage);
	}


	public void changeSecretMessage(string newMessage)
	{
		if(newMessage.Length > 0 && newMessage.Length<10)
		{
			SecretMessage = newMessage;
		}
		else
		
		{
			Debug.LogWarning("You Tried Sukkaa");

		}
	
	
	}
		public string GetSecretMessage()
	{
		return SecretMessage;
	}
}







