﻿using UnityEngine;
using System.Collections;

public class ScopeModifier : MonoBehaviour 
{
	public ScopePractice otherClass;
	public string MessageCopy;
	// Use this for initialization
	void Start () 
	{
		otherClass.scopeFunction ();
		otherClass.exists = true;
		otherClass.messageInABottle = "Hey";
		otherClass.checkIf = true;
		otherClass.notCheck = "Yes";
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Return))
		{
			otherClass.printSecretMessage();
		}
	
		if (Input.GetKeyDown (KeyCode.Backspace))
		{
			otherClass.changeSecretMessage("gfhf");
		}
	
		if (Input.GetKeyDown (KeyCode.Backslash)) 
		{
			MessageCopy = otherClass.GetSecretMessage();
		}
	
	}




}
