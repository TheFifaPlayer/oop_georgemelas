﻿using UnityEngine;
using System.Collections;

public class ConstantMovingPlayer : MonoBehaviour 
{
	public float Speed = 5f;
	public float JumpHeight = 50f;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position += Vector3.forward * Time.deltaTime * Speed;
		Jump ();

	}


	public void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			Speed -= 3f;
			Destroy(other.gameObject);
		}
}

	public void Jump()
	{
		if (Input.GetKey (KeyCode.Space)) 
		{
			GetComponent<Rigidbody>().AddForce(Vector3.up * JumpHeight);
			JumpHeight++;
		}
	}
}