﻿using UnityEngine;
using System.Collections;

public class ArrayRevision : MonoBehaviour 
{
	public int[] collectiveAges;
	public int ArraySize;
	public int currentIndex;
	
	// Use this for initialization
	void Start () 
	{
		
		collectiveAges = new int [ArraySize];
		 
	}
	
	// Update is called once per frame
	void Update () 
	{
		for(int index = 0; index < collectiveAges.Length; index++)
		{
			collectiveAges[index] = -1 ;
		}
	
		checkKeys();
		
	}
	
	

	public void checkKeys()
	{
		if(Input.GetKeyDown(KeyCode.UpArrow))
		{
			IncrementIndex();
		}

		if(Input.GetKeyDown(KeyCode.DownArrow))
		{
			DecreaseIndex(); 
		}
		
		if(Input.GetKeyDown(KeyCode.Space))
		{
			SetAge();
		}
	}


	public void SetAge()
	
	{
		collectiveAges[currentIndex] = 20;
		print("space Key was Pressed");
	}

	

	public void DecreaseIndex()
	
	{
		currentIndex --;
		if( currentIndex <0)
		{
			currentIndex =0;
			Debug.LogWarning ("Negative numbers are out of range! Currentindex set to 0");
		}
			
	}	
	
	
	

	
	public void IncrementIndex()
	
	{
		currentIndex ++;
		
	
		if( currentIndex > ArraySize - 1)	
			
		{
			currentIndex =ArraySize - 1;
			Debug.LogWarning ("Positive numbers are out of range! Currentindex set to 10");
		}
			
	}	
	
		

}




