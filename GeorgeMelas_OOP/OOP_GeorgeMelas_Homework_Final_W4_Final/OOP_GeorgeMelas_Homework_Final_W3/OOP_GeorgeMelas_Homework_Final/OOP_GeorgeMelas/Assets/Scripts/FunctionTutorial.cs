﻿using UnityEngine;
using System.Collections;

public class FunctionTutorial : MonoBehaviour 
{
	public string MyName = "George";
	public int MyAge = 17;
	public string MyGender = "Male";
	public string MyQuestion = "Games Design";
	public float Jumpforce;


	// Use this for initialization
	void Start () 
	{

		PrintID ();




	}
	
	// Update is called once per frame
	void Update () 
	{

		CheckKeyPresses();





	}
	

	public void PrintID()
	{
		print (MyName);
		print (MyAge);
		print (MyGender);
		print (MyQuestion);

	}


	public void CheckKeyPresses()
	
	{
	
		if (Input.GetKeyDown("w")) //Input.GetKey = Holding the key down // Input.getkeydown = pressing the key once
		{
			print ("W was pressed");
			MovePlayerFoward ();
		}
		
		if (Input.GetKeyDown("a"))
		{
			
			print ("A was pressed");
			MovePlayerBack ();
		}
		
		
		if (Input.GetKeyDown("s"))

		{
			print ("S was pressed");
			MovePlayerLeft ();
		}
		
		if (Input.GetKeyDown("d"))
		{
			print ("D was pressed");
			MovePlayerRight ();
		}

		if(Input.GetKeyDown("space"))
		{
			print ("space was pressed");
		   	Jump ();

		}
	}


		public void MovePlayerFoward()

		{
			print ("Player is moving forward");
			transform.Translate(Vector3.forward * Time.deltaTime);
		}


		public void MovePlayerBack()
		
		{
		print ("Player is moving back");
		transform.Translate (Vector3.back * Time.deltaTime);

	}
		public void MovePlayerRight()
			
		{
			print ("Player is moving Right");
			transform.Translate(Vector3.right * Time.deltaTime);
		}

		public void MovePlayerLeft()
			
		{
			print ("Player is moving Left");
			transform.Translate(Vector3.left * Time.deltaTime);
		}


	public void Jump()
	{
		print ("Player is Jumping");
		GetComponent<Rigidbody>().AddForce(Vector3.up * Jumpforce);

	}

		}

	




