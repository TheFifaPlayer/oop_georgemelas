using UnityEngine;
using System.Collections;
[System.Serializable]
public struct PokemonStats
{
	public int Level;
	public string Type;
	public string Creature;
	public string Power;
	public string Special;
	public string Taunt;
}


public class Pokemon : MonoBehaviour
{
	public PokemonStats myPokemon;
	// Use this for initialization
	void Start ()
	{
		PokemonCharmander ();
	}
	
	public void PokemonCharmander()
	{
		myPokemon.Level = 20;
		myPokemon.Type = "Fire";
		myPokemon.Creature = "Charmander";
		myPokemon.Power = "FireBlast";
		myPokemon.Special = "Flametail";
		myPokemon.Taunt = "Roar";

}

}