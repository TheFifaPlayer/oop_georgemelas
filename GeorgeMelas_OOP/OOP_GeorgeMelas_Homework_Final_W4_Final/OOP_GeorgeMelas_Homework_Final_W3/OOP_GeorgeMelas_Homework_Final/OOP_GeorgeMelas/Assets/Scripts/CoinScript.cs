﻿using UnityEngine;
using System.Collections;

public class CoinScript : MonoBehaviour 
{

	public string playerTag;
	public CoinManager otherCoinScript;

	public void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == playerTag)
		{
			otherCoinScript.AddScore();
			Destroy(this.gameObject);
		}
	}
}
