﻿using UnityEngine;
using System.Collections;

public class BoostPad : MonoBehaviour 
{
	public float ForceAmount;
	private Rigidbody playerBody;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	private void OnTriggerEnter(Collider col)
	{

	if(col.tag == "Player")
	{
			playerBody = col.GetComponent<Rigidbody>();
			applyForce(transform.forward,ForceAmount);
	}


}

	public void applyForce(Vector3 direction, float speed)
	{
		playerBody.AddForce(direction * speed,ForceMode.Impulse);
	}

}