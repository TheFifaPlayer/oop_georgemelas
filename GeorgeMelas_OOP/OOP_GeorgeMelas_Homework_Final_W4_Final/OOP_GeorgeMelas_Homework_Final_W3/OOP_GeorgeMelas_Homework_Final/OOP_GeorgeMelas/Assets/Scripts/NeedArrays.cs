﻿using UnityEngine;
using System.Collections;

public class NeedArrays : MonoBehaviour {
	public string studentOne;
	public string studentTwo;
	public string studentThree;
	public string studentFour;
	public string studentFive;
	public string studentSix;
	public string studentSeven;
	public string studentEight;
	public string studentNine;
	public string studentTen;
	// Use this for initialization
	void Start () 
	{
		studentOne = "George";
		studentTwo = "Bob";
		studentThree = "Harry";
		studentFour = "Barry";
		studentFive = "Zarry";
		studentSix = "Yabby";
		studentSeven = "Babby";
		studentEight = "Rabby";
		studentNine = "Crabby";
		studentTen = "Quabby";

		RenameStudents ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void RenameStudents()
	{
		studentOne = "Larry";
		studentTwo = "Perry";
		studentThree = "Gerry";
		studentFour = "Kickerz";
		studentFive = "Tekkers";
		studentSix = "Soccer";
		studentSeven = "Sabby";
		studentEight = "Wabby";
		studentNine = "Prabby";
		studentTen = "Tuabby";
	}

}

