﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public struct Address
{
	public int Housenumber;
	public string Streetname;
	public string Suburb;
	public int Postcode;
	public string State;
	public string Country;
}



public class StructTutorial : MonoBehaviour 
{
	public Address myAddress;

	// Use this for initialization
	void Start () 
	{
		SetStartingAddress ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown("space"))
		{
			ChangeAddress();
		}
	}


	public void SetStartingAddress ()
	{
		myAddress.Housenumber = 100;
		myAddress.Streetname = "Pacific Street";
		myAddress.Suburb = "St Lenoards";
		myAddress.Postcode = 2065;
		myAddress.State = "NSW";
		myAddress.Country = "Australia";
	
	}
	
	public void ChangeAddress()

	{
		myAddress.Housenumber = 123;
		myAddress.Streetname = "Fake Street";
		myAddress.Suburb = "St Lenoards";
		myAddress.Postcode = 2065;
		myAddress.State = "NSW";
		myAddress.Country = "Aussie";

	}

}


	

