﻿using UnityEngine;
using System.Collections;

public class ArrayTutorial : MonoBehaviour 
{

	public string[] studentNames;
	public int arraySize;

	// Use this for initialization
	void Start () 
	{
		SetNames ();
		printnames ();
		testLoop ();
		studentNames = new string[arraySize];
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void SetNames()
	{
//		studentNames [0] = "Stevie";
//		studentNames [1] = "Billy";
//		studentNames [2] = "Wayne";
//		studentNames [3] = "Steak";
//		studentNames [4] = "Bill";
//		studentNames [5] = "Hadoz";
		for(int index = 0; index <studentNames.Length; index++)
		{
			studentNames[index] = "student No." + index;
		}
	}


	public void printnames()
	{
	
//		print (studentNames [0]);
//		print (studentNames [1]);
//		print (studentNames [2]);
//		print (studentNames [3]);
//		print (studentNames [4]);
//		print (studentNames [5]);

		for (int index = 0; index <studentNames.Length; index++) 
		{
			print(studentNames[index]);
		}
	}

	public void testLoop()
	{
		for( int index = 0; index < 10 ;index++)
		{
			print(index);
			//index--;
		}
	}

}



