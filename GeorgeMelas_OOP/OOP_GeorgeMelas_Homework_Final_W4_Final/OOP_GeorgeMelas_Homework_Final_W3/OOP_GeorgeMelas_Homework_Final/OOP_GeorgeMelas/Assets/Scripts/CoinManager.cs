﻿using UnityEngine;
using System.Collections;

public class CoinManager : MonoBehaviour 
{
	public int totalScore;
	public int scoreAmount;

	public void AddScore()
	{
		scoreAmount += totalScore;
	}
}
