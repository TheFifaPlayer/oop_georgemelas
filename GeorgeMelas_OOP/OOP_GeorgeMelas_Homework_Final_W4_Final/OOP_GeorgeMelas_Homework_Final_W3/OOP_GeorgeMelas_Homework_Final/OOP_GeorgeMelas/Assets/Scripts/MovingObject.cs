﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour 
{
	public float MovementSpeed;
	public Rigidbody MyRigidBody;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		CheckKeys ();
	}




	public void CheckKeys()
	{
		if (Input.GetKey (KeyCode.W)) 
		{
			//MoveFoward ();
			move(Vector3.forward,MovementSpeed);
		}

		if (Input.GetKey (KeyCode.A)) 
		{
			//MoveLeft ();
			move(Vector3.left,MovementSpeed);
		}


		if (Input.GetKey (KeyCode.S)) 
		{
			//MoveBack();
			move(Vector3.back,MovementSpeed);

		}

		if (Input.GetKey (KeyCode.D)) 
		{
			//MoveRight ();
			move(Vector3.right,MovementSpeed);
		}

	}

//	public void MoveBack()
	//{
	//	GetComponent<Rigidbody> ().AddForce (Vector3.back * MovementSpeed);

	//}


//	public void MoveFoward()

//	{
		//GetComponent<Rigidbody> ().AddForce (Vector3.forward * MovementSpeed);
//	}

	///public void MoveLeft()
//	{
		//GetComponent<Rigidbody> ().AddForce (Vector3.left * MovementSpeed);
//	}


	//public void MoveRight()
	//{
		//GetComponent<Rigidbody> ().AddForce (Vector3.right * MovementSpeed);
	//}

	private void move(Vector3 direction, float forceAmount)
	{
		MyRigidBody.AddForce (direction * MovementSpeed);
	}


}



