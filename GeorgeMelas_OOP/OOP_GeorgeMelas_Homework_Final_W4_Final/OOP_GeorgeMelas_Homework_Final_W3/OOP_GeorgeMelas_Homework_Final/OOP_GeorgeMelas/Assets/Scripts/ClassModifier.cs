﻿using UnityEngine;
using System.Collections;

public class ClassModifier : MonoBehaviour 
{
	public ClassPractice otherClass;
	// Use this for initialization
	void Start () 
	{
		otherClass.PrintNumber ();
		otherClass.PrintFloat ();
		otherClass.PrintWord ();
		otherClass.PrintAnotherWord ();
		otherClass.PrintBool ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
