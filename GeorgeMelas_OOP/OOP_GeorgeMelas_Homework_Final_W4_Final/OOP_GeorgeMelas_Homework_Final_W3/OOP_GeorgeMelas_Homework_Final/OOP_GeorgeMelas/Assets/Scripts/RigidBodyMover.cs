using UnityEngine;
using System.Collections;
[System.Serializable]

public struct KeyBinds
{
	public KeyCode fowardkey;
	public KeyCode backkey;
	public KeyCode leftkey;
	public KeyCode rightkey;
	public KeyCode upKey;

}
public class RigidBodyMover : MonoBehaviour 
{

	public float movementspeed;
	public float jumpforce;
	public KeyBinds myKeyBinds;

	void Start(){
		KeyBindings ();
	}

	// Update is called once per frame
	void Update () 
	{
		CheckKeyPresses();

	}


	public void CheckKeyPresses()
	{

		if(Input.GetKey(myKeyBinds.fowardkey))
	{
			print("W key was pressed");
			MovePlayerFoward();
	}
	
		if(Input.GetKey(myKeyBinds.backkey))
		{
			print("A Key Was pressed");
			MovePlayerBack();
		}

	
		if(Input.GetKey(myKeyBinds.leftkey))
		{
			print("S Key Was Pressed");
			MovePlayerLeft();
		}
	
		if(Input.GetKey(myKeyBinds.rightkey))
		{
			print("D Key Was Pressed");
			MovePlayerRight();
		}
	
		
		if(Input.GetKeyDown(KeyCode.Space))
		{
			print("Space Key Was Pressed");
			Jump();

		}

	}


		public void MovePlayerFoward()
	{	print ("Player is Moving Foward");
		GetComponent<Rigidbody>().AddForce(Vector3.forward * movementspeed);

	}

		public void MovePlayerBack()
	{	print ("Player is Moving Back");
		GetComponent<Rigidbody>().AddForce(Vector3.back * movementspeed);
		
	}

		public void MovePlayerLeft()
	{	print ("Player is Moving Left");
		GetComponent<Rigidbody>().AddForce(Vector3.left * movementspeed);
		
	}


	public void MovePlayerRight()
	{	print ("Player is Moving Right");
		GetComponent<Rigidbody>().AddForce(Vector3.right * movementspeed);
		
	}


	public void Jump()
	{	print ("Player is Jumping");
		GetComponent<Rigidbody>().AddForce(Vector3.up * jumpforce);
	}


	public void KeyChecks ()
	{

		if(Input.GetKey(myKeyBinds.fowardkey))
	
		{
			MovePlayerFoward();

		}
	}

	private void KeyBindings(){
		myKeyBinds.fowardkey = KeyCode.W;
		myKeyBinds.leftkey = KeyCode.A;
		myKeyBinds.backkey = KeyCode.S;
		myKeyBinds.rightkey = KeyCode.D;
	}
}
